---
layout: single
title: Fermented Oranges
date: 2014-02-15 00:31
author: Owen Priestley
comments: true
permalink: /portfolio/hogtown_enquirer/fermented_oranges/
author_profile: true
read_time: true
header:
  image: fermented_oranges.png
---
We're still living the unemployed life: I don’t have an alarm clock. I rise with the low winter sun; by the chatter of black squirrels on the sill; or with the bitter chill that permeates every crevice and drags you back into the frozen world of consciousness.

We looked at two houses this week that were perhaps the worst houses I have ever seen; including the houses I have seen in films, on television and in terrifying, disturbing nightmares.

"The previous tenants made holes in the hardwood floor to hide drugs." The landlady introduced us to the front room, shuffling around in snow-caked Crocs. "And they smoked.. without ashtray."

OK, so the floor had a hole in it, that was fixable. The bathroom, and the mental scarring it incurred, was not. It was in the basement. Down a heavily soiled staircase that, even the landlady Cristina admitted, needed repair, through a dark passageway and into a claustrophobic, tiled room that I wouldn't doubt had been a dungeon at some point. Everything was grimy, and whilst the light was very poor, I reckon there was unflushed business in the bowl.

"Yes, you will need to crouch in the shower." Cristina looked at me, and at that point I was convinced we would never be leaving the basement. 

But we did escape, and it's not a bad life; without responsibilities we can stay up as late as we want. Last night we went to a party in a hat makers shop; it wasn't quite as outrageous as it sounds, but there were free cocktails and a half-decent folk act in the shop window.

<img alt="image" src="tumblr_inline_n10g18v8pN1rde5ly.jpg" width="100%" />
<h2 class="img">Observing the eerie icicles outside the house of doom / the Horseshoe Tavern.</h2>

Next door to the hat shop sits the Horseshoe Tavern, a balls out American rock &amp; roll dive bar, complete with custom Harley, pool cues for fightin' and an autographed photograph of Link Wray on the wall. A stark contrast to the roots music and haberdashery we'd been subjected to just two doors down.

The Horseshoe is currently celebrating it's sixty fifth anniversary with live music every night during the month of February, and I guess the bands and clientele could be best described as a mixed bag. The first band were by far the best, not only because they were called Fermented Oranges, but because they were followed by two of the most outrageously cringe inducing acts I have ever witnessed.

One of which reminded me of a young _Maroon 5_, and not in a good way; three clean cut, hip thrusting, pre-pubescent teens playing the kind of grooves that'd make Jesus proud, whilst Lou Reed slowly turns in his grave. The other were like a Canadian version of the Script, and I'll leave it at that.

We left the Horseshoe with the bitter taste of betrayal in our mouths; how could a bonafide badass like Link Wray have played on the same stage as the Kings of Cringe? Fortunately, what followed what was probably the best burger I will ever eat at a place called the Burger's Priest, where portions seem to be inhumanely colossal.

Still feeling deflated by the anticlimax at the Horseshoe and weighed down by copious beef patties, it seemed all was lost, but then on the recommendation of the prior establishment's doorman, we tried a venue called _The Cameron_.

<img alt="image" src="tumblr_inline_n10gl3c1vI1rde5ly.jpg" width="100%" />
<h2 class="img">I thought that comic sans was the worst font, until I picked up this free biker mag / Harlan Pepper at the Grand Ole Cameron.</h2>

It was two in the morning and Harlan Pepper had just started playing. With a lead singer that looked suspiciously like a young Dylan, it was safe to say that things were going to improve drastically. I don't know exactly how old they were, but either way, they were far too young to be so good. 

Inevitably, they played some excellent Dylan covers. Also some obscure surf tracks and plenty of rock and roll. We will be visiting _The Cameron_ again very soon.

<img alt="image" src="tumblr_inline_n10gc0YprG1rde5ly.jpg" width="100%" />
<h2 class="img">Token shot of Jess and Danny demonstrating how cold it is. This time they're at the Harbourfront.</h2>